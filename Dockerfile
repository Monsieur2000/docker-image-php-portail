FROM phpdockerio/php80-fpm:latest
WORKDIR "/simplea"

RUN apt-get update; \
    apt-get -y --no-install-recommends install \
        php8.0-cgi \ 
        php8.0-gd \ 
        php8.0-intl \ 
        php8.0-mongodb \ 
        php8.0-oauth \ 
        php8.0-solr \
        locales; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

RUN locale-gen fr_CH.UTF-8
RUN update-locale LANG=fr_CH.UTF-8
ENV LANG fr_CH.UTF-8
ENV LANGUAGE fr_CH.UTF-8
ENV LC_ALL fr_CH.UTF-8